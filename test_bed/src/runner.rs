///! TODO(dixie): Separate gameplay code out into the `gameplay` module as much as possible.
///! Ideally, this module should just show how to configure and use Rollercoaster without any
///! additional cruft.
use crate::gameplay::*;
use ggez::{
    Context, GameResult,
    graphics::{self, Color, DrawMode, DrawParam, MeshBuilder, Mesh, Rect},
    input::keyboard::{KeyCode, KeyMods},
};
use rollercoaster::{Rollercoaster, TickInput, InputData};
use std::time::{Duration, Instant};

const FPS: f32 = 60.0;

fn load_state(game: &mut Game, state: &GameState) {
    game.local = state.local.clone();
    game.remote = state.remote.clone();
}

fn tick_game(game: &mut Game, inputs: TickInput<Input>) {
    game.tick(inputs.clone());
}

fn gen_state(game: &Game) -> GameState {
    game.into()
}

pub struct GameState {
    local: Player,
    remote: Player,
}

impl From<&Game> for GameState {
    fn from(game: &Game) -> Self {
        Self {
            local: game.local.clone(),
            remote: game.remote.clone(),
        }
    }
}

#[derive(Clone)]
pub struct Input {
    pub left_held: bool,
    pub right_held: bool,
    pub attack_pressed: bool,
}

impl Default for Input {
    fn default() -> Self {
        Self {
            left_held: false,
            right_held: false,
            attack_pressed: false,
        }
    }
}

impl Input {
    fn clear_temp_state(&mut self) {
        self.attack_pressed = false;
    }
}

impl InputData for Input {
    const SIZE_AS_BYTES: usize = 3;
    fn as_be_bytes(&self, out: &mut [u8]) {
        out[0] = if self.left_held { 1 } else { 0 };
        out[1] = if self.right_held { 1 } else { 0 };
        out[2] = if self.attack_pressed { 1 } else { 0 };
    }

    fn from_be_bytes(bytes: &[u8]) -> Self {
        Self {
            left_held: bytes[0] == 1,
            right_held: bytes[1] == 1,
            attack_pressed: bytes[2] == 1,
        }
    }
}

pub struct GameRunner {
    prev: Instant,
    pub role: Role,
    pub rollercoaster: Rollercoaster<Game, GameState, Input>,
    pub inputs_provided: bool,
    pub game: Game,
    pub inputs: Input,
    pub ready: bool,
}

impl GameRunner {
    pub fn new(role: Role, ctx: &mut Context) -> Self {
        let roll_config = rollercoaster::Config {
            bind_addr: role.pick("127.0.0.1:8080", "127.0.0.1:8081"),
            connect_addr: role.pick("127.0.0.1:8081", "127.0.0.1:8080"),
            max_rollback: 8, //TODO: role.pick(8, 2),
            delay: role.pick(0, 5),
            load_fn: load_state,
            tick_fn: tick_game,
            state_fn: gen_state,
            prediction_fn: None,
        };

        let game = Game::new(role, ctx);
        let rollercoaster = Rollercoaster::new(&game, roll_config)
            .expect("Failed to initialise Rollercoaster");

        Self {
            role,
            game,
            rollercoaster,
            prev: Instant::now(),
            inputs_provided: false,
            inputs: Default::default(),
            ready: false,
        }
    }

    pub fn update(&mut self, _ctx: &mut Context) -> GameResult {
        let timeout = Duration::from_millis(10);
        self.rollercoaster.do_work(&mut self.game, timeout).unwrap();

        let threshold = Duration::from_secs_f32(1.0 / FPS);
        let now = Instant::now();
        let mut elapsed = now - self.prev;

        // TODO(dixie): Delet this. Filthy hack to test shit computer handling
        //let threshold = match self.role {
        //    Role::Player1 => threshold,
        //    Role::Player2 => threshold * 2,
        //};

        while elapsed >= threshold {
            if self.rollercoaster.ready() {
                match self.role {
                    Role::Player1 => println!("P1"),
                    Role::Player2 => println!("P2"),
                }

                self.ready = true;
                self.prev = now;
                elapsed -= threshold;

                self.rollercoaster.provide_local_inputs(self.inputs.clone());
                self.inputs.clear_temp_state();
                let inputs = self.rollercoaster.get_inputs(&mut self.game);
                self.game.tick(inputs);

                self.inputs_provided = false;
                self.rollercoaster.end_tick(GameState::from(&self.game));
            } else {
                self.ready = false;
                println!("{} >= {}", elapsed.as_secs_f32(), threshold.as_secs_f32());
                break;
            }
        }
        
        Ok(())
    }

    pub fn draw_local(&mut self, ctx: &mut Context) -> GameResult {
        self.game.draw_local(ctx)
    }

    pub fn draw_remote(&mut self, ctx: &mut Context) -> GameResult {
        self.game.draw_remote(ctx)
    }

    pub fn key_down_event(&mut self, _ctx: &mut Context, key: KeyCode, _mods: KeyMods, repeat: bool) {
        if repeat { return; }

        match self.role {
            Role::Player1 => match key {
                KeyCode::Right => self.inputs.right_held = true,
                KeyCode::Left => self.inputs.left_held = true,
                KeyCode::Space | KeyCode::Z => self.inputs.attack_pressed = true,
                _ => (),
            }
            Role::Player2 => match key {
                KeyCode::D => self.inputs.right_held = true,
                KeyCode::A => self.inputs.left_held = true,
                KeyCode::Space | KeyCode::Z => self.inputs.attack_pressed = true,
                _ => (),
            }
        }
    }

    pub fn key_up_event(&mut self, _ctx: &mut Context, key: KeyCode, _mods: KeyMods) {
        match self.role {
            Role::Player1 => match key {
                KeyCode::Right => self.inputs.right_held = false,
                KeyCode::Left => self.inputs.left_held = false,
                _ => (),
            }
            Role::Player2 => match key {
                KeyCode::D => self.inputs.right_held = false,
                KeyCode::A => self.inputs.left_held = false,
                _ => (),
            }
        }
    }
}

pub struct Game {
    role: Role,
    local: Player,
    remote: Player,

    body: Mesh,
    local_atk: Mesh,
    remote_atk: Mesh,
}

impl Game {
    fn new(role: Role, ctx: &mut Context) -> Self {
        let body_bounds = Rect::new(
            -(BODY_SIZE[0] as f32 / 2.0),
            -BODY_SIZE[1] as f32,
            BODY_SIZE[0] as f32,
            BODY_SIZE[1] as f32,
        );

        let p1_atk_bounds = Rect::new(
            body_bounds.x + body_bounds.w,
            body_bounds.y + body_bounds.h / 3.0 - ATK_SIZE as f32 / 2.0,
            ATK_SIZE as f32,
            ATK_SIZE as f32,
        );

        let p2_atk_bounds = Rect::new(
            body_bounds.x - ATK_SIZE as f32,
            body_bounds.y + body_bounds.h / 3.0 - ATK_SIZE as f32 / 2.0,
            ATK_SIZE as f32,
            ATK_SIZE as f32,
        );

        Self {
            role,
            local: Player::new(role),
            remote: Player::new(-role),

            body: MeshBuilder::new()
                .rectangle(DrawMode::stroke(1.0), body_bounds, graphics::Color::WHITE)
                .unwrap()
                .build(ctx)
                .unwrap(),
            local_atk: MeshBuilder::new()
                .rectangle(DrawMode::stroke(1.0), role.pick(p1_atk_bounds, p2_atk_bounds), graphics::Color::WHITE)
                .unwrap()
                .build(ctx)
                .unwrap(),
            remote_atk: MeshBuilder::new()
                .rectangle(DrawMode::stroke(1.0), (-role).pick(p1_atk_bounds, p2_atk_bounds), graphics::Color::WHITE)
                .unwrap()
                .build(ctx)
                .unwrap(),
        }
    }

    fn tick(&mut self, inputs: TickInput<Input>) {
        self.local.update(&inputs.local);
        self.remote.update(&inputs.remote);

        let local_attacking = self.local.state == PlayerState::Attacking;
        let local_atk_state = self.local.atk_state;
        let local_atk = self.local.atk_rect(self.role);

        let remote_attacking = self.remote.state == PlayerState::Attacking;
        let remote_atk_state = self.remote.atk_state;
        let remote_atk = self.remote.atk_rect(-self.role);

        if local_attacking && local_atk_state == AttackState::Active {
            if local_atk.overlaps(self.remote.body_rect()) 
            {
                self.remote.state = PlayerState::Dead;
            } else if remote_attacking && remote_atk_state != AttackState::Startup && local_atk.overlaps(remote_atk) {
                self.local.state = PlayerState::Dead;
                self.remote.state = PlayerState::Dead;
            }
        }

        if remote_attacking && remote_atk_state == AttackState::Active {
            if remote_atk.overlaps(self.local.body_rect()) {
                self.local.state = PlayerState::Dead;
            } 
        }
    }

    fn draw_local(&mut self, ctx: &mut Context) -> GameResult {
        let col = graphics::Color::WHITE;

        let local_param = DrawParam::default().dest(self.local.draw_pos()).color(col);
        graphics::draw(ctx, &self.body, local_param)?;
        if self.local.state == PlayerState::Attacking {
            graphics::draw(ctx, &self.local_atk, local_param)?;
        }
        Ok(())
    }

    fn draw_remote(&mut self, ctx: &mut Context) -> GameResult {
        let col = Color::from_rgb(255, 0, 0);
        let remote_param = DrawParam::default().dest(self.remote.draw_pos()).color(col);
        graphics::draw(ctx, &self.body, remote_param)?;
        if self.remote.state == PlayerState::Attacking {
            graphics::draw(ctx, &self.remote_atk, remote_param)?;
        }
        Ok(())
    }
}


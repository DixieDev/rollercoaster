///! In here, you will mainly find the GGEZ boilerplate to set up a game loop and route inputs
///! through to the GameRunner. It's divided this way so we can run two instances of the game at
///! once, and have them connected to one another to make sure that the lib at least works over a
///! connection with the minimum possible latency.
///!
///! Check the `runner` module to see how to use Rollercoaster in a game, or `gameplay` to see
///! various types and behaviour that the game itself implements. 

mod runner;
mod gameplay;

use runner::{GameRunner, Input};
use gameplay::Role;
use ggez::{
    self,
    Context, ContextBuilder, GameResult,
    conf::{WindowMode, WindowSetup},
    event::{self, EventHandler},
    graphics::{self, DrawParam, Text},
    input::keyboard::{KeyCode, KeyMods},
};

fn main() {
    let (mut ctx, event_loop) = ContextBuilder::new("Rollercoaster Test", "DixiE")
        .window_setup(WindowSetup {
            title: "Rollercoaster in Action".to_owned(),
            vsync: true,
            ..Default::default()
        })
        .window_mode(WindowMode {
            width: 640.0,
            height: 480.0,
            ..WindowMode::default()
        })
        .build()
        .unwrap();

    let game = TestApp::new(&mut ctx);
    event::run(ctx, event_loop, game);
}

struct TestApp {
    paused: bool,
    runners: [GameRunner; 2],
}

impl TestApp {
    fn new(ctx: &mut Context) -> Self {
        Self {
            paused: false,
            runners: [
                GameRunner::new(Role::Player1, ctx),
                GameRunner::new(Role::Player2, ctx),
            ]
        }
    }

    fn debug_char(&self, inputs: &[Option<Input>], input: usize) -> char {
        match &inputs[input] {
            Some(input) => if input.left_held { '<' } else if input.right_held { '>' } else { 'O' },
            None => '_'
        }
    }
}

impl EventHandler<ggez::GameError> for TestApp {
    fn update(&mut self, ctx: &mut Context) -> GameResult {
        if !self.paused {
            for runner in self.runners.iter_mut() {
                runner.update(ctx)?;
            }
        }
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        graphics::clear(ctx, graphics::Color::BLACK);

        for runner in self.runners.iter_mut() {
            runner.draw_remote(ctx)?;
        }
        
        for runner in self.runners.iter_mut() {
            runner.draw_local(ctx)?;
        }

        let p1_local = self.runners[0].rollercoaster.local_inputs();
        let p1_remote = self.runners[0].rollercoaster.remote_inputs();
        let p2_local = self.runners[1].rollercoaster.local_inputs();
        let p2_remote = self.runners[1].rollercoaster.remote_inputs();

        graphics::draw(
            ctx,
            &Text::new(format!(
r##"P1 Tick: {}
P1 Earliest Unknown: {}
P1 Ready: {}
P2 Tick: {}
P2 Earliest Unknown: {}
P2 Ready: {}
P1 L Inputs: {} {} {} {} {} {} {} {}
P2 R Inputs: {} {} {} {} {} {} {} {}
P1 R Inputs: {} {} {} {} {} {} {} {}
P2 L Inputs: {} {} {} {} {} {} {} {}
"##,
                self.runners[0].rollercoaster.tick(),
                self.runners[0].rollercoaster.earliest_unknown(),
                self.runners[0].ready,
                self.runners[1].rollercoaster.tick(),
                self.runners[1].rollercoaster.earliest_unknown(),
                self.runners[1].ready,
                self.debug_char(p1_local, 0), 
                self.debug_char(p1_local, 1), 
                self.debug_char(p1_local, 2), 
                self.debug_char(p1_local, 3), 
                self.debug_char(p1_local, 4), 
                self.debug_char(p1_local, 5), 
                self.debug_char(p1_local, 6), 
                self.debug_char(p1_local, 7), 

                self.debug_char(p2_remote, 0), 
                self.debug_char(p2_remote, 1), 
                self.debug_char(p2_remote, 2), 
                self.debug_char(p2_remote, 3), 
                self.debug_char(p2_remote, 4), 
                self.debug_char(p2_remote, 5), 
                self.debug_char(p2_remote, 6), 
                self.debug_char(p2_remote, 7), 

                self.debug_char(p1_remote, 0), 
                self.debug_char(p1_remote, 1), 
                self.debug_char(p1_remote, 2), 
                self.debug_char(p1_remote, 3), 
                self.debug_char(p1_remote, 4), 
                self.debug_char(p1_remote, 5), 
                self.debug_char(p1_remote, 6), 
                self.debug_char(p1_remote, 7), 

                self.debug_char(p2_local, 0), 
                self.debug_char(p2_local, 1), 
                self.debug_char(p2_local, 2), 
                self.debug_char(p2_local, 3), 
                self.debug_char(p2_local, 4), 
                self.debug_char(p2_local, 5), 
                self.debug_char(p2_local, 6), 
                self.debug_char(p2_local, 7), 
            )),
            DrawParam::default().dest([10.0, 10.0]),
        )?;

        graphics::present(ctx)
    }

    fn key_down_event(&mut self, ctx: &mut Context, key: KeyCode, mods: KeyMods, repeat: bool) {
        if let KeyCode::P = key {
            if !repeat { self.paused = !self.paused; }
        }

        if !self.paused {
            for runner in self.runners.iter_mut() {
                runner.key_down_event(ctx, key, mods, repeat);
            }
        }
    }

    fn key_up_event(&mut self, ctx: &mut Context, key: KeyCode, mods: KeyMods) {
        if !self.paused {
            for runner in self.runners.iter_mut() {
                runner.key_up_event(ctx, key, mods);
            }
        }
    }
}

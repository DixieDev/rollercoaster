use crate::runner::Input;
use nalgebra as na;

pub const ATK_STARTUP: u32 = 1;
pub const ATK_ACTIVE: u32 = 1;
pub const ATK_RECOVERY: u32 = 1;

pub const BODY_SIZE: [i32; 2] = [64, 256];
pub const ATK_SIZE: i32 = 64;

#[derive(Clone, Copy, Eq, PartialEq)]
pub struct IRect {
    pub x: i32,
    pub y: i32,
    pub w: i32,
    pub h: i32,
}

impl IRect {
    pub fn overlaps(self, other: Self) -> bool {
        !(self.x + self.w < other.x
            || self.x > other.x + other.w
            || self.y + self.h < other.y
            || self.y > other.y + other.h)
    }
}

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum PlayerState {
    Idle,
    Walking,
    Attacking,
    Dead,
}

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum AttackState {
    Startup,
    Active,
    Recovery,
}

#[derive(Clone)]
pub struct Player {
    pub position: na::Point2<i32>,
    pub state: PlayerState,
    pub atk_state: AttackState,
    pub counter: u32,
}

impl Player {
    pub fn new(role: Role) -> Self {
        Self {
            position: match role {
                Role::Player1 => [100, 400].into(),
                Role::Player2 => [540, 400].into(),
            },
            state: PlayerState::Idle,
            atk_state: AttackState::Startup,
            counter: 0,
        }
    }

    pub fn update(&mut self, input: &Input) {
        if let PlayerState::Attacking = self.state {
            self.counter += 1;
            if self.counter < ATK_STARTUP {
                self.atk_state = AttackState::Startup;
            } else if self.counter < ATK_STARTUP + ATK_ACTIVE {
                self.atk_state = AttackState::Active;
            } else if self.counter < ATK_STARTUP + ATK_ACTIVE + ATK_RECOVERY {
                self.atk_state = AttackState::Recovery;
            } else {
                self.state = PlayerState::Idle;
            }
        }

        let mut ms = 0;
        match self.state {
            PlayerState::Idle | PlayerState::Walking => {
                if input.right_held {
                    ms += 5;
                } 
                if input.left_held {
                    ms -= 5;
                }
                if input.attack_pressed {
                    ms = 0;
                    self.state = PlayerState::Attacking;
                    self.atk_state = AttackState::Startup;
                    self.counter = 0;
                } else if ms == 0 {
                    self.state = PlayerState::Idle;
                }
            },
            _ => (),
        }

        if ms != 0 {
            self.state = PlayerState::Walking;
            self.walk(ms);
        }
    }

    fn walk(&mut self, off: i32) {
        self.position.x += off;
        if self.position.x < 0 {
            self.position.x = 0;
        }

        if self.position.x >= 640 {
            self.position.x = 639;
        }
    }

    pub fn draw_pos(&self) -> na::Point2<f32> {
        na::Point2::new(self.position.x as f32, self.position.y as f32)
    }

    pub fn body_rect(&self) -> IRect {
        IRect {
            x: self.position.x - BODY_SIZE[0]/2,
            y: self.position.y - BODY_SIZE[1],
            w: BODY_SIZE[0],
            h: BODY_SIZE[1],
        }
    }

    pub fn atk_rect(&self, role: Role) -> IRect {
        IRect {
            x: self.position.x + role.pick(BODY_SIZE[0]/2, -BODY_SIZE[0]/2 - ATK_SIZE),
            y: self.position.y - BODY_SIZE[1]/2 - ATK_SIZE/2,
            w: ATK_SIZE,
            h: ATK_SIZE,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum Role {
    Player1,
    Player2,
}

impl Role {
    pub fn pick<T>(self, p1: T, p2: T) -> T {
        match self {
            Self::Player1 => p1,
            Self::Player2 => p2,
        }
    }
}

impl std::ops::Neg for Role {
    type Output = Role;
    fn neg(self) -> Self {
        match self {
            Self::Player1 => Self::Player2,
            Self::Player2 => Self::Player1,
        }
    }
}


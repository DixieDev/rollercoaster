///! TODO(dixie): Split some of this out into separate structs and files. The input buffers in
///!              particular could go elsewhere, and probably the send and recv functions too.

use std::convert::TryInto;
use std::io;
use std::net::{UdpSocket, ToSocketAddrs};
use std::time::{Duration, Instant};

pub type Result<T=()> = std::result::Result<T, RollbackError>;

/// Repeatedly evaluates and pushes a given expression into a new vec a set number of times.
///
/// Example:
///
/// ```
/// let my_vec: Vec<Option<UnclonableStruct>> = vec_with!(None, count);
/// assert_eq!(count, my_vec.len());
/// for i in 0..my_vec.len() {
///     assert_eq!(None, my_vec[i]);
/// }
/// ```
macro_rules! vec_with {
    ($value:expr, $num:expr) => {
        {
            let mut vec = Vec::with_capacity($num);
            for _ in 0..$num {
                vec.push($value);
            }
            vec
        }
    }
}

struct RiftCheckData {
    local_tick: u64,
    remote_tick: u64,
    remote_unknown: u64,
}

struct RiftDetection {
}

impl RiftDetection {
    pub fn new() -> Self {
        Self {
        }
    }

    pub fn rifting(&mut self, check_data: RiftCheckData) -> bool {
        let RiftCheckData { local_tick, remote_tick, remote_unknown } = check_data;

        // If the remote player is way ahead of us, it's not our job to stall for the resync.
        if remote_tick > local_tick {
            return false;
        }

        // Determine local rift, and estimate current remote rift based on available information
        let local_rift = local_tick - remote_tick;
        let remote_known = if remote_unknown > 0 { remote_unknown - 1 } else { 0 };
        let remote_rift = if remote_tick > remote_known { remote_tick - remote_known } else { 0 };

        // If our local rift is bigger than the remote's rift, we need to stall to let them catch
        // up a bit.
        if local_rift > remote_rift {
            let rift_diff = local_rift - remote_rift;
            if rift_diff > 1 {
                eprintln!("Out of sync with rift_diff {}. Local tick {}, remote tick {}, remote_unknown {}", rift_diff, local_tick, remote_tick, remote_unknown);
                true
            } else {
                false
            }
        } else {
            false
        } 
    }
}

/// Header for the packets we send across the network. 
#[derive(Clone)]
struct Header {
    /// The sequential ID for the tick this packet describes.
    tick: u64,

    /// The earliest tick we don't yet know about from the peer. For example; if we are on tick 5, 
    /// and have received ticks 1, 2, and 4 from our peer, this will be set to 3.
    earliest_unknown: u64,
}

impl Header {
    fn new() -> Self {
        Self {
            tick: 0,
            earliest_unknown: 0,
        }
    }

    fn size(&self) -> usize {
        16
    }

    /// Copies the content into the provided slice. The copied data will be packed without
    /// padding, and is output in big endian format, since network people like it the most.
    fn as_be_bytes(&self, out: &mut [u8]) {
        if out.len() < self.size() {
            panic!("Too few bytes in output buffer");
        }

        out[0..8].copy_from_slice(&self.tick.to_be_bytes());  
        out[8..16].copy_from_slice(&self.earliest_unknown.to_be_bytes());  
    }

    /// Copies data from the buffer into the header's fields, accounting for endianness.
    fn from_be_bytes(&mut self, bytes: &[u8]) {
        self.tick = u64::from_be_bytes(bytes[0..8].try_into().unwrap());
        self.earliest_unknown = u64::from_be_bytes(bytes[8..16].try_into().unwrap());
    }
}

/// The recommended action to take, as returned by `Rollercoaster`'s private `recv` function.
enum RecvAction {
    Rollback(u64),
    None,
}

/// Lots of things can go wrong when configuring Rollercoaster and while it does it's business over
/// the network. In most cases, there's not much you can do but you should at least be able to log 
/// the failure reason.
#[derive(Debug)]
pub enum RollbackError {
    IOErr(io::Error),
    DelayExceedsMaxRollback,
    MaxRollbackOverflow,
    UnexpectedEndOfPacket,
    Unknown,
}

impl From<io::Error> for RollbackError {
    fn from(e: io::Error) -> Self {
        Self::IOErr(e)
    }
}

/// A key trait in your `Rollercoaster` implementation. Implement this for the type you use for
/// each frame's input data, and it should work seamlessly with `Rollercoaster`'s interfaces.
pub trait InputData: Clone + Default {
    const SIZE_AS_BYTES: usize;
    fn as_be_bytes(&self, out: &mut [u8]);
    fn from_be_bytes(bytes: &[u8]) -> Self;
}

/// Stores a sparse vec of inputs. If one is missing, it means it has yet to be received.
struct Inputs<Input: InputData>(Vec<Option<Input>>);

impl<Input: InputData> Inputs<Input> {
    fn new(capacity: usize) -> Self {
        Self(vec_with!(None, capacity))
    }

    fn size_for_capacity(capacity: usize) -> usize {
        (Input::SIZE_AS_BYTES + 1) * capacity
    }

    fn size_as_bytes(&self) -> usize {
        (Input::SIZE_AS_BYTES + 1) * self.0.len()
    }

    fn as_be_bytes(&self, out: &mut [u8], tick: u64, rollback: u8, delay: u8) {
        assert!(out.len() >= self.size_as_bytes());
        let tick = tick as usize;
        let rollback = rollback as usize;
        let delay = delay as usize;

        let mut cursor = 0;
        let start = if tick < rollback { 0 } else { tick + 1 - rollback };
        for current in start..=tick {
            //TODO(dixie): Remove this branch???
            if false { //current < delay {
                // First byte per input slot indicates whether or not the input is known. 0
                // indicates false, while 1 indicates true.
                out[cursor] = 1;
                cursor += 1;

                // Since the current tick is before the delay period is even over, we just send the
                // default input to the peer.
                Input::default().as_be_bytes(&mut out[cursor..cursor+Input::SIZE_AS_BYTES]);
                cursor += Input::SIZE_AS_BYTES;
            } else {
                // Delay period is over, so we can send actual inputs
                let idx = current % rollback;
                match self.0[idx].as_ref() {
                    Some(input) => {
                        out[cursor] = 1;
                        cursor += 1;
                        input.as_be_bytes(&mut out[cursor..cursor+Input::SIZE_AS_BYTES]);
                        cursor += Input::SIZE_AS_BYTES;
                    },
                    None => {
                        out[cursor] = 0;
                        cursor += 1 + Input::SIZE_AS_BYTES;
                    }
                }
            }
        }
    } 

    fn from_be_bytes(&mut self, bytes: &[u8]) {
        let len = self.0.len();
        self.0.clear();
        for i in 0..len {
            let idx = i * (1+Input::SIZE_AS_BYTES);

            self.0.push(match bytes[idx] {
                0 => None,
                _ => Some(Input::from_be_bytes(&bytes[idx+1..idx+1+Input::SIZE_AS_BYTES]))
            });
        }
    }
}

/// Contains the local and remote input data for the current frame. You can retrieve this by
/// calling `Rollercoaster::get_inputs`.
#[derive(Clone)]
pub struct TickInput<Input: InputData> {
    pub local: Input,
    pub remote: Input,
}

/// Configuration for a `Rollercoaster` session. 
pub struct Config<Game, State, Input, ConnectAddr, BindAddr>
where Input: InputData,
      ConnectAddr: ToSocketAddrs,
      BindAddr: ToSocketAddrs,
{
    /// The address we're connecting to
    pub connect_addr: ConnectAddr,

    /// The address we're binding to
    pub bind_addr: BindAddr,

    /// The limit on the number of rollback frames. Increasing this will allow for poorer
    /// connections without increased input delay or stuttering, but the rollbacks will get more
    /// noticeable. Must be identical for both players. 
    ///
    /// If inputs are not received for a frame as old as `max_rollback + delay`, then the game has 
    /// no choice but to wait for the inputs to be received before proceeding. For a 60fps game, it 
    /// is recommended not to have this higher than 8. 
    pub max_rollback: u8,

    /// The input delay to add on the local machine. Increasing this will reduce how noticable the
    /// rollbacks are on poorer connections, at the cost of user inputs being delayed locally.
    /// It is recommended that you ask your users to configure their delay for themselves, with a
    /// suggestion based on their opponent's connection quality.
    ///
    /// If inputs are not received for a frame as old as `max_rollback + delay`, then the game has
    /// no choice but to wait for the inputs before it can proceed.
    pub delay: u8,

    /// A callback that loads the `State` data back into `Game`. 
    pub load_fn: fn(&mut Game, &State),

    /// A callback that ticks the game with the provided user inputs. This must be deterministic,
    /// otherwise both players will desync.
    pub tick_fn: fn(&mut Game, TickInput<Input>),

    /// A callback that returns a `State` object based on your full `Game` data. You want this to
    /// be small, but to contain everything that is required to roll back your game to a previous
    /// state and then be fast-forwarded back to the current tick with new input data. 
    ///
    /// For example, you will definitely want to record player positions and health, but you might
    /// not care so much about the positions of particles used only for background graphics.
    pub state_fn: fn(&Game) -> State,

    /// An optional callback where you can provide your own input prediction for the remote player
    /// on the current tick. Unless you have some crazy new ideas, it's recommended you just 
    /// provide `None` here, which will default to repeating the remote player's previous inputs.
    pub prediction_fn: Option<fn(&Game) -> Input>,
}

/// Specifies whether we know about a certain tick's input.
enum InputAwareness {
    Known(u64),
    Unknown,
}

/// The big boy himself. See the crate-level documentation for a quick start guide.
pub struct Rollercoaster<Game, State, Input: InputData> {
    tick: u64,
    max_rollback: u8,
    delay: u8,
    states: Vec<Option<State>>,
    out_of_sync: bool,
    rift_detection: RiftDetection,

    local_inputs: Inputs<Input>,
    local_input_awareness: Vec<InputAwareness>,

    remote_inputs: Inputs<Input>,
    remote_input_awareness: Vec<InputAwareness>,
    last_remote_input: Option<Input>,

    socket: UdpSocket,

    send_header: Header,
    send_buf: Vec<u8>,

    recv_header: Header,
    recv_inputs: Inputs<Input>,
    recv_buf: Vec<u8>,

    load_fn: fn(&mut Game, &State),
    tick_fn: fn(&mut Game, TickInput<Input>),
    state_fn: fn(&Game) -> State,
    prediction_fn: Option<fn(&Game) -> Input>,
}

impl<Game, State, Input: InputData> Rollercoaster<Game, State, Input> {
    /// Initialises Rollercoaster ready to communicate with the remote player. You are expected to
    /// determine the connect address ahead of time, ideally via some kind of lobby system, but
    /// possibly through users pasting IP addresses to one another to allow for play even once your
    /// lobby servers eventually die.
    pub fn new<ConnectAddr, BindAddr>(game: &Game, config: Config<Game, State, Input, ConnectAddr, BindAddr>) -> Result<Self> 
    where ConnectAddr: ToSocketAddrs,
          BindAddr: ToSocketAddrs,
    {
        //TODO(dixie): After delay is fixed, this requirement might not be necessary
        assert!(config.delay < config.max_rollback, "Input delay must less than the max rollback");
        assert!(config.max_rollback > 0, "Maximum rollback must be more than 0");

        if config.delay >= config.max_rollback {
            return Err(RollbackError::DelayExceedsMaxRollback);
        }

        let socket = UdpSocket::bind(config.bind_addr)?;
        socket.connect(config.connect_addr)?;
        socket.set_nonblocking(true)?;

        let max_rollback = config.max_rollback as usize;

        let mut states = vec_with!(None, max_rollback);
        states[0] = Some((config.state_fn)(game));

        let send_header = Header::new();
        let send_buf = vec![0u8; send_header.size() + Inputs::<Input>::size_for_capacity(max_rollback)];
        let recv_buf = send_buf.clone();

        Ok(Self {
            tick: 0,
            max_rollback: config.max_rollback,
            delay: config.delay,
            states,
            out_of_sync: false,
            rift_detection: RiftDetection::new(),

            local_inputs: Inputs::new(max_rollback),
            local_input_awareness: vec_with!(InputAwareness::Unknown, max_rollback),

            remote_inputs: Inputs::new(max_rollback),
            remote_input_awareness: vec_with!(InputAwareness::Unknown, max_rollback),
            last_remote_input: None,

            socket,
            send_header,
            send_buf,

            recv_header: Header::new(),
            recv_inputs: Inputs::new(max_rollback),
            recv_buf,

            load_fn: config.load_fn,
            tick_fn: config.tick_fn,
            state_fn: config.state_fn,
            prediction_fn: config.prediction_fn,
        })
    }

    /// Returns a slice over the local input buffer. Not massively useful if you're not trying to
    /// debug things. Note that it is a ring buffer with length equal to the provided max_rollback.
    pub fn local_inputs(&self) -> &[Option<Input>] {
        &self.local_inputs.0
    }

    /// Returns a slice over the remote input buffer. Not massively useful if you're not trying to
    /// debug things. Note that it is a ring buffer with length equal to the provided max_rollback.
    pub fn remote_inputs(&self) -> &[Option<Input>] {
        &self.remote_inputs.0
    }

    /// An index into the local and remote input buffers for the current frame. Note that usually
    /// we are one frame ahead of any provided input, so you'll usually only want to use this when
    /// inserting data. 
    fn current_tick_idx(&self) -> usize {
        ((self.tick - self.delay as u64) % self.max_rollback as u64) as usize
    }

    /// Returns the current tick number.
    pub fn tick(&self) -> u64 {
        self.tick
    }

    /// The tick number of the oldest remote input that we currently don't know.
    pub fn earliest_unknown(&self) -> u64 {
        self.send_header.earliest_unknown
    }

    /// Performs the work of sending and receiving input data, reloading states and advancing
    /// through the ticks using your provided `load_fn` and `tick_fn` whenever the game can be
    /// resynchronised based on the remote player's inputs. 
    ///
    /// You want to call this frequently, regardless of the value returned from calls to `ready()`.
    pub fn do_work(&mut self, game: &mut Game, timeout: Duration) -> Result {
        let start = Instant::now();
        let mut elapsed = Duration::from_millis(0);
        let mut prev_elapsed = elapsed;
        let mut poll_delay = Duration::from_millis(0);

        while elapsed < timeout {
            let dt = elapsed - prev_elapsed;
            if poll_delay > dt {
                poll_delay -= dt;
            } else {
                poll_delay = Duration::from_millis(0);
            }

            if poll_delay == Duration::from_millis(0) {
                self.send()?;

                if let RecvAction::Rollback(rollback_tick) = self.recv()? {
                    // If we need to rollback, then reload state from the given tick and rerun each tick
                    // until the game is caught back up.
                    if rollback_tick < self.tick {
                        let rollback_length = self.tick - rollback_tick + 1;
                        if rollback_length >= self.max_rollback as u64 {
                            self.out_of_sync = true;
                        }

                        let idx = rollback_tick as usize % self.states.len();
                        if let Some(state) = self.states[idx].as_ref() { 
                            // Reload all game state
                            (self.load_fn)(game, state);

                            // Determine how many ticks we need to resimulate
                            let catchup_ticks = self.tick - rollback_tick;
                            self.tick = rollback_tick;

                            // Perform ticks
                            for _ in 0..catchup_ticks {
                                (self.tick_fn)(game, self.get_inputs(game));
                                self.end_tick((self.state_fn)(game));
                            }
                        } //TODO(dixie): Handle lack of available state. Pretty sure that's a significant error, even if it shouldn't really be feasible.
                    }
                }

                let remaining = if timeout > elapsed { Duration::from_millis(0) } else { timeout - elapsed };
                if remaining >= Duration::from_millis(1) {
                    poll_delay = Duration::from_millis(1);
                }
            } 

            prev_elapsed = elapsed;
            elapsed = Instant::now() - start;
        }

        Ok(())
    }

    fn send(&mut self) -> Result {
        self.send_header.earliest_unknown = self.find_earliest_unknown();

        // Don't bother sending anything if they've already received the current tick's info
        if self.tick <= self.recv_header.earliest_unknown {
            return Ok(());
        }

        self.send_header.tick = self.tick-1;

        let pivot = self.send_header.size();
        self.send_header.as_be_bytes(&mut self.send_buf[0..pivot]);
        self.local_inputs.as_be_bytes(&mut self.send_buf[pivot..], self.tick-1, self.max_rollback, self.delay);

        self.socket.send(&self.send_buf)?;
        Ok(())
    }

    /// Resolves the tick number of the oldest remote input that we don't yet know.
    fn find_earliest_unknown(&self) -> u64 {
        let rollback = self.max_rollback as u64;
        let start = if self.tick < rollback { 0 } else { self.tick - rollback };
        for tick in start..=self.tick {
            let idx = (tick % rollback) as usize;
            match self.remote_input_awareness[idx] {
                InputAwareness::Known(known_tick) => if known_tick < tick {
                    return tick; 
                },
                InputAwareness::Unknown => return tick,
            }
            if self.remote_inputs.0[idx].is_none() {
                return tick;
            }
        }

        self.tick+1
    }

    fn recv(&mut self) -> Result<RecvAction> {
        match self.socket.recv(&mut self.recv_buf) {
            Ok(count) if count < self.recv_buf.len() => Err(RollbackError::UnexpectedEndOfPacket),
            Ok(_) => {
                // Deserialise header
                self.recv_header.from_be_bytes(&self.recv_buf[0..self.recv_header.size()]);

                let rollback = self.max_rollback as u64;
                let recv_tick = self.recv_header.tick;

                // Verify that the received tick is not too far ahead or too far behind, ignoring
                // them if they are.
                // TODO(dixie): Is this meant to be using > or >= ?
                if self.tick > recv_tick && self.tick - recv_tick > rollback {
                    return Err(RollbackError::MaxRollbackOverflow);
                } else if self.tick < recv_tick && recv_tick - self.tick > rollback {
                    return Err(RollbackError::MaxRollbackOverflow);
                }

                // Deserialise inputs
                self.recv_inputs.from_be_bytes(&self.recv_buf[self.recv_header.size()..]);

                // Perform rift detection if recv tick is newest
                if recv_tick >= self.send_header.earliest_unknown {
                    self.out_of_sync = self.rift_detection.rifting(RiftCheckData {
                        local_tick: self.tick,
                        remote_tick: recv_tick, 
                        remote_unknown: self.recv_header.earliest_unknown,
                    });
                }

                // We want to record the earliest tick that has had missing inputs filled, so we
                // know where we need to rollback to.
                let mut earliest_fill = None;

                // For each received input, insert it into our list of local inputs.
                let first_tick = if recv_tick < rollback { 0 } else { recv_tick + 1 - rollback };
                if first_tick > self.tick {
                    return Ok(RecvAction::None);
                }

                let end = u64::min(recv_tick+1, rollback);
                for recv_idx in 0..end as usize {
                    let tick = first_tick + recv_idx as u64;
                    let local_idx = (tick % rollback) as usize;
                    let delayed_tick = tick + self.delay as u64;
                    
                    let should_fill = match self.remote_input_awareness[local_idx] {
                        InputAwareness::Known(known_tick) if known_tick < delayed_tick => true,
                        InputAwareness::Known(_) => false,
                        InputAwareness::Unknown => true,
                    };
                    if should_fill && self.recv_inputs.0[recv_idx].is_some() {
                        self.remote_input_awareness[local_idx] = InputAwareness::Known(delayed_tick);

                        if earliest_fill.is_none() {
                            earliest_fill = Some(delayed_tick);
                        }
                    } 
                    self.remote_inputs.0[local_idx] = self.recv_inputs.0[recv_idx].take();
                }

                match earliest_fill {
                    Some(tick) if tick < self.tick => Ok(RecvAction::Rollback(tick)),
                    _ => Ok(RecvAction::None),
                }
            },
            Err(e) => {
                match e.kind() {
                    io::ErrorKind::WouldBlock => Ok(RecvAction::None),
                    io::ErrorKind::ConnectionReset => Ok(RecvAction::None),
                    _ => Err(RollbackError::IOErr(e)),
                }
            }
        }
    }

    /// Returns true if you can safely begin the next tick, and false if you should wait.
    pub fn ready(&self) -> bool {
        // TODO(dixie): If we're reaching the rollback limits sync-wise, force a resync instead of 
        //              just preventing a single tick.

        let max_rollback = self.max_rollback as u64;
        let delay = self.delay as u64;

        let recv_earliest_unknown = self.recv_header.earliest_unknown;
        let local_earliest_unknown = self.send_header.earliest_unknown;
        let next_tick = self.tick + 1;

        let should_stall = {
            // Don't advance game state until the player lagging behind has caught up
            self.out_of_sync 

            // Check if we're about to override our local input data for the earliest tick that the
            // remote player doesn't know yet on our next tick
            || (recv_earliest_unknown <= self.tick && next_tick - recv_earliest_unknown >= max_rollback)

            // Similarly, check if we'd override our local input data for the oldest tick that
            // we've not received remote input data for. 
            || (next_tick >= max_rollback && next_tick - max_rollback >= local_earliest_unknown)
        };

        //println!("{}: nect_tick {}, recv {}, local {}", should_stall, next_tick, recv_earliest_unknown, local_earliest_unknown);

        !should_stall
    }

    /// Retrieves inputs for the current tick. If the remote user's input has not yet been received, 
    /// it is predicted by either your prediction function or by mimicking their input on the
    /// previous tick if you did not provide one.
    ///
    /// Make sure you call provide_local_inputs before calling this, or else you will eventually
    /// end up with a panic!
    pub fn get_inputs(&mut self, game: &Game) -> TickInput<Input> {
        // Retrieve local player's input for the current tick. If it's missing, the lib user has
        // probably forgotten to call provide_local_inputs (or it's a bug of our own), so we panic.
        // If we're still in the delay period, just treat the input as default.
        let local = if self.tick >= self.delay as u64{
            let tick_idx = self.current_tick_idx();
            println!("get_inputs called on tick {} (idx {})", self.tick, tick_idx);
            match self.local_input_awareness[tick_idx] {
                InputAwareness::Known(known_tick) if known_tick == self.tick => {
                    self.local_inputs.0[tick_idx].clone().unwrap()
                },

                InputAwareness::Known(known_tick) => panic!("Input awareness for tick {} (idx {}) is known for tick {} instead", self.tick, tick_idx, known_tick),

                _ => panic!("Missing local inputs for tick {}. Did you forget a call to provide_local_inputs?", self.tick),
            }
        } else {
            Input::default()
        };

        // Get remote player's input for current tick. If it's not been received yet, attempt to
        // predict what it will be. If were still in the delay period, just treat the input as
        // default.
        let remote = if self.tick >= self.delay as u64 {
            let tick_idx = self.current_tick_idx();
            match self.remote_input_awareness[tick_idx] {
                InputAwareness::Known(known_tick) if known_tick == self.tick => {
                    self.remote_inputs.0[tick_idx].clone().unwrap()
                },
                _ => self.predict_input(game),
            }
        } else {
            Input::default()
        };

        self.last_remote_input = Some(remote.clone());

        TickInput { local, remote }
    }

    pub fn latency(&self) -> u64 {
        if self.tick >= self.send_header.earliest_unknown {
            self.tick - self.send_header.earliest_unknown
        } else {
            0
        }
    }

    /// Provide input from your local user. If your delay is 0, this will be returned to you by a
    /// call to `get_inputs` immediately, or at a later tick otherwise.
    pub fn provide_local_inputs(&mut self, input: Input) {
        let idx = (self.tick % self.max_rollback as u64) as usize;
        self.local_inputs.0[idx] = Some(input);
        self.local_input_awareness[idx] = InputAwareness::Known(self.tick+self.delay as u64);
        println!("Provided local input for tick {} (idx {})", self.tick + self.delay as u64, idx);
    }

    /// Report that you've finished everything for your current tick, and provide the current state
    /// so that it can be saved for rollbacks in future.
    pub fn end_tick(&mut self, current_state: State) {
        // Advance tick
        self.tick += 1;

        // Record tick state
        let states_idx = self.tick as usize % self.states.len();
        self.states[states_idx] = Some(current_state);
    }

    /// Invokes the provided input prediction callback if one has been provided, otherwise it
    /// returns the previous input. If there's not been a previous input, then it returns
    /// `Input::default()`.
    fn predict_input(&self, game: &Game) -> Input {
        match self.prediction_fn {
            // Call the lib user's provided prediction function
            Some(func) => func(game),

            // Reuse previous input; only default if there is none
            None => match self.last_remote_input.as_ref() {
                Some(input) => input.clone(),
                None => Input::default(),
            }
        }
    }
}
